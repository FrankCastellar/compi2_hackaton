package cup2;
import java_cup.runtime.*;
%%
%class Lexer
%line
%column
%cup
%public
%{   
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    private Symbol symbol(int type, Object varue) {
        return new Symbol(type, yyline, yycolumn, varue);
    }
%}
LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
dec_int_lit = 0 | [1-9][0-9]*
dec_string_lit = "\\\""[^\\]*"\\\""
dec_id = [A-Za-z_][A-Za-z_0-9]*
%%
<YYINITIAL> {
    "+"                { return symbol(sym.PLUS); }
    "-"                { return symbol(sym.MINUS); }
    "*"                { return symbol(sym.TIMES); }
    "/"                { return symbol(sym.DIVIDE); }
    "#"                { return symbol(sym.UNARY); }
    "="                { return symbol(sym.EQUALS); }
    "."                { return symbol(sym.DOT); }

    {dec_int_lit}      { return symbol(sym.NUMBER, new Double(yytext())); }

    {dec_id}       { return symbol(sym.NAME, yytext() );}

    {WhiteSpace}       { /* just skip what was found, do nothing */ }

    {dec_string_lit}   {return symbol(sym.STRING, yytext());}
}
[^]                    { throw new Error("Illegal character <"+yytext()+">"); }
