import Properties.*;
import Valores.*;
import cup.Lexer;
import cup.parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Franklin on 12-12-14.
 */
public class Main {
    public static void main(String args[]) throws Exception, SemanticException {
        parser p = new parser(new Lexer(new FileReader("src/sample/example.txt")));
        Property prop = (Property) p.parse().value;
        ((FormProperty)prop).createForm().setVisible(true);


    }
}
