package Valores;

/**
 * Created by furan on 12/12/14.
 */
public class NumberValue extends Value {
    double numero;

    public NumberValue(double numero) {
        this.numero = numero;
    }

    public double getNumero() {
        return numero;
    }

    public void setNumero(double numero) {
        this.numero = numero;
    }
}
