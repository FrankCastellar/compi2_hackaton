package Valores;

import Properties.Property;

import java.util.List;

/**
 * Created by furan on 12/12/14.
 */
public class ListValue extends Value {
    List<Property> lista;

    public ListValue(List<Property> lista) {
        this.lista = lista;
    }

    public List<Property> getLista() {
        return lista;
    }

    public void setLista(List<Property> lista) {
        this.lista = lista;
    }
}
