package Valores;

import Properties.Property;

import java.util.List;

/**
 * Created by furan on 12/12/14.
 */
public class ArrayValue extends Value {
    List<Value> arreglo;

    public ArrayValue(List<Value> arreglo) {
        this.arreglo = arreglo;
    }

    public List<Value> getArreglo() {
        return arreglo;
    }

    public void setArreglo(List<Value> arreglo) {
        this.arreglo = arreglo;
    }
}
