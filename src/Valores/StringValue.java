package Valores;

/**
 * Created by furan on 12/12/14.
 */
public class StringValue extends Value {
    String cadena;

    public StringValue(String cadena) {
        this.cadena = cadena.substring(1, cadena.length() -1);
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
}
