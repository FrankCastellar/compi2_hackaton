package Valores;

import event.statement.AssignStatement;
import event.statement.StatementNode;

/**
 * Created by Franklin on 12-13-14.
 */
public class EventValue extends Value{
    AssignStatement statementNode;

    public EventValue(StatementNode statementNode) {
        this.statementNode = (AssignStatement)statementNode;
    }

    public AssignStatement getStatementNode() {
        return statementNode;
    }


}
