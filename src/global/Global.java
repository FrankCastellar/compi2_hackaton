package global;

import Valores.EventValue;
import event.expression.ExpressionNode;
import event.statement.AssignStatement;
import event.type.*;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Franklin on 12-13-14.
 */
public class Global {
    private static Map<String, JComponent> components;
    private static Map<String, EventValue> buttonEventValues;

    static{
        components = new HashMap<String, JComponent>();
        buttonEventValues =new HashMap<String, EventValue>();
    }

    public static JComponent getComponentById(String id){
        return components.get(id);
    }

    public static void ActionToPerform(String id) throws Exception {
        EventValue values = getEventValueById(id);
        if(values == null){
            return;
        }
        AssignStatement assignStatement = values.getStatementNode();
        if(assignStatement == null){
            return;
        }
        String originComponentName = assignStatement.getComponentName();
        String originPropertyName = assignStatement.getPropertyName();

        ExpressionNode value = assignStatement.getValue();

        if(value == null){
            return;
        }
        ExpressionValue resulting = value.evaluate();
        JComponent originComponent = getComponentById(originComponentName);
        if(originPropertyName.toUpperCase().equals("X")){
            originComponent.setLocation(new Integer(((NumberValue)resulting).getNumber().intValue()), originComponent.getY());
            return;
        }

        if(originPropertyName.toUpperCase().equals("Y")){
            originComponent.setLocation(originComponent.getX(), new Integer(((NumberValue)resulting).getNumber().intValue()));
            return;
        }

        if(originPropertyName.toUpperCase().equals("WIDTH")){
            originComponent.setSize(new Integer(((NumberValue) resulting).getNumber().intValue()), originComponent.getHeight());
            return;
        }

        if(originPropertyName.toUpperCase().equals("HEIGHT")){
            originComponent.setSize(originComponent.getWidth(), new Integer(((NumberValue)resulting).getNumber().intValue()));
            return;
        }

        if(originPropertyName.toUpperCase().equals("ID")){
            originComponent.setName(((StringValue)resulting).getText());
            return;
        }


        if(originPropertyName.toUpperCase().equals("VALUE")){
            if(originComponent instanceof JTextField){
                if(resulting instanceof NumberValue){
                    ((JTextField)originComponent).setText(String.valueOf(((NumberValue) resulting).getNumber()));
                    return;
                }

                if(resulting instanceof StringValue){
                    ((JTextField)originComponent).setText(String.valueOf(((StringValue) resulting).getText()));
                    return;
                }
            }

            if(originComponent instanceof JButton){
                if(resulting instanceof NumberValue){
                    ((JButton)originComponent).setText(String.valueOf(((NumberValue) resulting).getNumber()));
                    return;
                }

                if(resulting instanceof StringValue){
                    ((JButton)originComponent).setText(String.valueOf(((StringValue) resulting).getText()));
                    return;
                }
            }

            if(originComponent instanceof JLabel){
                if(resulting instanceof NumberValue){
                    ((JLabel)originComponent).setText(String.valueOf(((NumberValue) resulting).getNumber()));
                    return;
                }

                if(resulting instanceof StringValue){
                    ((JLabel)originComponent).setText(String.valueOf(((StringValue) resulting).getText()));
                    return;
                }
            }
        }
    }

    public static EventValue getEventValueById(String id){
        return buttonEventValues.get(id);
    }

    public static Map<String, JComponent> getComponents() {
        return components;
    }

    public static void setComponents(Map<String, JComponent> components) {
        Global.components = components;
    }

    public static Map<String, EventValue> getButtonEventValues() {
        return buttonEventValues;
    }

    public static void setButtonEventValues(Map<String, EventValue> buttonEventValues) {
        Global.buttonEventValues = buttonEventValues;
    }
}
