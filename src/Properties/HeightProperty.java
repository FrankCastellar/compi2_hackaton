package Properties;

import Valores.NumberValue;

/**
 * Created by furan on 12/12/14.
 */
public class HeightProperty extends Property {
    public double getHeight() {
        if(propertyValue instanceof NumberValue) return ((NumberValue) propertyValue).getNumero();
        else return 0;
    }
}
