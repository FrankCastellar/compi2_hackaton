package Properties;

import Valores.ListValue;

import java.awt.*;

/**
 * Created by furan on 12/12/14.
 */
public class SizeProperty extends Property {

    public Dimension getDimension() throws SemanticException {
        Dimension d = new Dimension();

        if(propertyValue instanceof ListValue){
            double width = 0, height = 0;
            for(Property p : ((ListValue) propertyValue).getLista()){
                if(p instanceof WidthProperty){
                    width = ((WidthProperty)p).getWidth();
                }

                if(p instanceof HeightProperty){
                    height = ((HeightProperty)p).getHeight();
                }
            }
            d.setSize(width, height);
        }else throw new SemanticException("Se esperaba una lista para la propiedad Size");

        return d;
    }
}
