package Properties;

import Valores.ListValue;

import java.awt.*;

/**
 * Created by furan on 12/12/14.
 */
public class PositionProperty extends Property {

    public Point getPoint() throws SemanticException {
        Point pt = new Point();

        if(propertyValue instanceof ListValue){
            double x = 0, y = 0;
            for(Property p : ((ListValue) propertyValue).getLista()){
                if(p instanceof XProperty){
                    x = ((XProperty)p).getX();
                }

                if(p instanceof YProperty){
                    y = ((YProperty)p).getY();
                }
            }
            pt.setLocation(x, y);
        }else throw new SemanticException("Se esperaba una lista para la propiedad Size");

        return pt;
    }
}
