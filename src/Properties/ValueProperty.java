package Properties;

import Valores.StringValue;

/**
 * Created by furan on 12/12/14.
 */
public class ValueProperty extends Property {
    public String getValue() {
        String val = "";
        if(propertyValue instanceof StringValue){
            val = ((StringValue) propertyValue).getCadena();
        }
        return val;
    }
}
