package Properties;

import Valores.*;
import cup2.Lexer;
import cup2.parser;
import event.statement.AssignStatement;
import event.statement.StatementNode;
import global.Global;

import java.io.FileReader;
import java.io.StringReader;

/**
 * Created by furan on 12/12/14.
 */
public class OnClickProperty extends Property {
    @Override
    public void setPropertyValue(Value propertyValue) throws Exception {
        StringValue propertyVal = (StringValue)propertyValue;
        String value = propertyVal.getCadena();
        parser p = new parser(new Lexer(new StringReader(value)));
        StatementNode statement = (StatementNode) p.parse().value;
        EventValue eventValue = new EventValue(statement);
        super.setPropertyValue(eventValue);
    }

    public EventValue getEventValue(){
        return (EventValue)this.propertyValue;
    }
}
