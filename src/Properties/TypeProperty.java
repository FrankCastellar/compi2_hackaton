package Properties;

import Valores.StringValue;

/**
 * Created by furan on 12/12/14.
 */
public class TypeProperty extends Property {
    public String getType() {
        String type = null;
        if(propertyValue instanceof StringValue){
            type = ((StringValue) propertyValue).getCadena();
        }
        return type;
    }
}
