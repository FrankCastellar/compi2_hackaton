package Properties;

import Valores.ArrayValue;
import Valores.EventValue;
import Valores.ListValue;
import Valores.Value;
import event.statement.StatementNode;
import global.Global;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by furan on 12/12/14.
 */
public class ControlsProperty extends Property {
    public List<JComponent> getComponents() throws SemanticException {
        List<JComponent> jcs = new ArrayList<JComponent>();
        if(propertyValue instanceof ArrayValue){
            for(Value v : ((ArrayValue) propertyValue).getArreglo()){
                if(v instanceof ListValue){
                    String type = null;
                    String id = null;
                    String val = "";
                    Point p = new Point();
                    Dimension d = new Dimension();
                    EventValue eventValue = null;

                    for(Property pr : ((ListValue) v).getLista()){
                        if(pr instanceof TypeProperty){
                            type = ((TypeProperty)pr).getType();
                        }

                        if(pr instanceof IdProperty){
                            id = ((IdProperty)pr).getId();
                        }

                        if(pr instanceof ValueProperty){
                            val = ((ValueProperty)pr).getValue();
                        }

                        if(pr instanceof PositionProperty){
                            p = ((PositionProperty) pr).getPoint();
                        }

                        if(pr instanceof SizeProperty){
                            d = ((SizeProperty) pr).getDimension();
                        }

                        if(pr instanceof OnClickProperty){
                            eventValue = ((OnClickProperty) pr).getEventValue();
                        }
                    }

                    if(type != null && id != null){
                        JComponent jc = null;
                        if ("Button".equals(type)){
                            jc = new JButton();
                            ((JButton)jc).setText(val);
                            Global.getButtonEventValues().put(id, eventValue);
                            final String finalID = id;
                            jc.addMouseListener(new java.awt.event.MouseAdapter() {
                                public void mouseClicked(java.awt.event.MouseEvent evt) {
                                    try {
                                        Global.ActionToPerform(finalID);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } else if ("TextBox".equals(type)){
                            jc = new JTextField();
                            ((JTextField)jc).setText(val);

                        } else if ("Label".equals(type)){
                            jc = new JLabel();
                            ((JLabel)jc).setText(val);
                        }else{
                            throw new SemanticException("No se reconocio el tipo de control " + type);
                        }
                        jc.setName(id);
                        jc.setLocation(p);
                        jc.setSize(d);
                        Global.getComponents().put(id, jc);
                        jcs.add(jc);
                    }


                }
            }
        }else throw new SemanticException("Se esperaba un arreglo en Controls");
        return jcs;
    }
}
