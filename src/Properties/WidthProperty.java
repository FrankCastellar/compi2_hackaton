package Properties;

import Valores.NumberValue;

/**
 * Created by furan on 12/12/14.
 */
public class WidthProperty extends Property {
    public double getWidth() {
        if(propertyValue instanceof NumberValue) return ((NumberValue) propertyValue).getNumero();
        else return 0;
    }
}
