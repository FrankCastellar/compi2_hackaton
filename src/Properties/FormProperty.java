package Properties;

import Valores.ListValue;


import javax.swing.*;

/**
 * Created by furan on 12/12/14.
 */
public class FormProperty extends Property {


    public JFrame createForm() throws SemanticException {
        JFrame f = new JFrame();
        f.setLayout(null);


        if(propertyValue instanceof ListValue){
            for(Property p : ((ListValue) propertyValue).getLista()){

                if(p instanceof PositionProperty){
                    f.setLocation(((PositionProperty)p).getPoint());
                }

                if(p instanceof SizeProperty){
                    f.setSize(((SizeProperty)p).getDimension());
                }

                if(p instanceof ControlsProperty){
                    for(JComponent jc : ((ControlsProperty)p).getComponents()){
                        f.add(jc);
                    }
                }
            }
        }else throw new SemanticException("Se esperaba una lista como valor de la propiedad form");

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return f;
    }
}
