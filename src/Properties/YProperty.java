package Properties;

import Valores.NumberValue;

/**
 * Created by furan on 12/12/14.
 */
public class YProperty extends Property {
    public double getY() {
        if(propertyValue instanceof NumberValue) return ((NumberValue) propertyValue).getNumero();
        else return 0;
    }
}
