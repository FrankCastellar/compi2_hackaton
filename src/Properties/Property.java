package Properties;

import Valores.Value;

/**
 * Created by furan on 12/12/14.
 */
public abstract class Property {
    Value propertyValue;


    public Value getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(Value propertyValue) throws Exception {
        this.propertyValue = propertyValue;
    }
}
