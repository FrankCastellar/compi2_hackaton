package Properties;

import Valores.StringValue;

/**
 * Created by furan on 12/12/14.
 */
public class IdProperty extends Property {
    public String getId() {
        String id = null;
        if(propertyValue instanceof StringValue){
            id = ((StringValue) propertyValue).getCadena();
        }
        return id;
    }
}
