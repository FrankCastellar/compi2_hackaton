package cup;
import java_cup.runtime.*;
%%
%class Lexer
%line
%column
%cup
%public
%{   
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    private Symbol symbol(int type, Object varue) {
        return new Symbol(type, yyline, yycolumn, varue);
    }
%}
LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
dec_int_lit = 0 | [1-9][0-9]*
dec_string_lit = \"(\\.|[^\"])*\"
dec_id = [A-Za-z_][A-Za-z_0-9]*
%%
<YYINITIAL> {
    "+"                { return symbol(sym.PLUS); }
    "-"                { return symbol(sym.MINUS); }
    "*"                { return symbol(sym.TIMES); }
    "/"                { return symbol(sym.DIVIDE); }
    "#"                { return symbol(sym.UNARY); }
    "="                { return symbol(sym.EQUALS); }
    "\""               { return symbol(sym.DOBLEQUOTE); }
    ","                { return symbol(sym.COMMA); }
    "{"                { return symbol(sym.LEFTCURLY); }
    "}"                { return symbol(sym.RIGHTCURLY); }
    "["                { return symbol(sym.LEFTBRACKET); }
    "]"                { return symbol(sym.RIGHTBRACKET); }
    "."                { return symbol(sym.DOT); }

    "X:"                { return symbol(sym.X); }
    "x:"                { return symbol(sym.X); }
    "Y:"                { return symbol(sym.Y); }
    "y:"                { return symbol(sym.Y); }
    "Form:"             { return symbol(sym.FORM); }
    "Size:"             { return symbol(sym.SIZE); }
    "Position:"         { return symbol(sym.POSITION); }
    "Controls:"         { return symbol(sym.CONTROLS); }
    "Width:"            { return symbol(sym.WIDTH); }
    "Height:"           { return symbol(sym.HEIGHT); }
    "Type:"             { return symbol(sym.TYPE); }
    "Value:"             { return symbol(sym.VALUE); }
    "OnClick:"           { return symbol(sym.ONCLICK); }
    "Id:"                { return symbol(sym.ID); }

    {dec_string_lit}   {return symbol(sym.STRING, yytext());}

    {dec_int_lit}      { return symbol(sym.NUMBER, new Integer(yytext())); }

    {dec_id}           { return symbol(sym.NAME, yytext() );}

    {WhiteSpace}       { /* just skip what was found, do nothing */ }
}
[^]                    { throw new Error("Illegal character <"+yytext()+">"); }
