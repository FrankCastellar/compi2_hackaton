package event.statement;

import event.expression.ExpressionNode;

/**
 * Created by Franklin on 12-13-14.
 */
public class AssignStatement extends StatementNode {
    String componentName;
    String propertyName;

    ExpressionNode value;


    public AssignStatement(String componentName, String propertyName, ExpressionNode value){
        this.componentName = componentName;
        this.propertyName = propertyName;
        this.value = value;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public ExpressionNode getValue() {
        return value;
    }

    public void setValue(ExpressionNode value) {
        this.value = value;
    }
}
