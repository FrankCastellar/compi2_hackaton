/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package event.expression;

import event.type.ExpressionValue;
import event.type.NumberValue;

/**
 *
 * @author Franklin
 */
public class SubNode extends BinaryOperatorNode{

    public SubNode(ExpressionNode left, ExpressionNode right) {
        super(left, right);
    }

    @Override
    public ExpressionValue evaluate() throws Exception {
        return new NumberValue(
                ((NumberValue)left.evaluate()).getNumber()
                        -
                        ((NumberValue)right.evaluate()).getNumber()
        );
    }
}
