/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package event.expression;

import event.type.ExpressionValue;

/**
 *
 * @author Franklin
 */
public abstract class ExpressionNode {
    public abstract ExpressionValue evaluate() throws Exception;
}
