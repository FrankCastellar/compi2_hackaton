/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package event.expression;

/**
 *
 * @author Franklin
 */
public abstract class BinaryOperatorNode extends ExpressionNode{
    protected ExpressionNode right;
    protected   ExpressionNode left;

    public BinaryOperatorNode(ExpressionNode left, ExpressionNode right) {
        this.right = right;
        this.left = left;
    }

    public ExpressionNode getRight() {
        return right;
    }

    public void setRight(ExpressionNode right) {
        this.right = right;
    }

    public ExpressionNode getLeft() {
        return left;
    }

    public void setLeft(ExpressionNode left) {
        this.left = left;
    }
    
}
