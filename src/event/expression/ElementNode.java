package event.expression;

import event.type.*;
import global.Global;

import javax.swing.*;

/**
 * Created by Franklin on 12-13-14.
 */
public class ElementNode extends ExpressionNode {
    String componentName;
    String propertyName;
    public ElementNode(String componentName, String propertyName) {
        super();
        this.componentName = componentName;
        this.propertyName = propertyName;
    }

    @Override
    public ExpressionValue evaluate() throws Exception {
        JComponent component = Global.getComponentById(this.componentName);
        if(this.propertyName.toUpperCase().equals("X")){
            return new NumberValue(new Double(component.getX()));
        }

        if(this.propertyName.toUpperCase().equals("Y")){
            return new NumberValue(new Double(component.getY()));
        }

        if(this.propertyName.toUpperCase().equals("WIDTH")){
            return new NumberValue(new Double(component.getWidth()));
        }

        if(this.propertyName.toUpperCase().equals("HEIGHT")){
            return new NumberValue(new Double(component.getHeight()));
        }

        if(this.propertyName.toUpperCase().equals("ID")){
            return new StringValue(component.getName());
        }

        if(this.propertyName.toUpperCase().equals("VALUE")){
            if(component instanceof JLabel){
                return new StringValue(((JLabel)component).getText());
            }

            if(component instanceof JButton){
                return new StringValue(((JButton)component).getText());
            }

            if(component instanceof JTextField){
                return new StringValue(((JTextField)component).getText());
            }
        }

        return null;
    }
}
