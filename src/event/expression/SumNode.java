/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package event.expression;

import event.type.*;

/**
 *
 * @author Franklin
 */
public class SumNode extends BinaryOperatorNode{

    public SumNode(ExpressionNode left, ExpressionNode right) {
        super(left, right);
    }

    @Override
    public ExpressionValue evaluate() throws Exception {
        if(left.evaluate() instanceof NumberValue && right.evaluate() instanceof NumberValue){
            return new NumberValue(((NumberValue) left.evaluate()).getNumber()+ ((NumberValue) right.evaluate()).getNumber());
        }else if(left.evaluate() instanceof StringValue && right.evaluate() instanceof StringValue){
            return new StringValue(((StringValue) left.evaluate()).getText() + ((StringValue) right.evaluate()).getText());
        }

        throw new Exception("No sea maule.");
    }

}
