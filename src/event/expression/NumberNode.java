package event.expression;

import event.type.ExpressionValue;
import event.type.NumberValue;

/**
 * Created by Franklin on 12-13-14.
 */
public class NumberNode extends ExpressionNode {
    Double num;
    public NumberNode(Double n) {
        super();
        this.num = n;
    }

    @Override
    public ExpressionValue evaluate() {
        return new NumberValue(this.num);
    }
}
