package event.expression;

import event.expression.ExpressionNode;
import event.type.*;

/**
 * Created by Franklin on 12-13-14.
 */
public class StringNode extends ExpressionNode {
    String text;
    public StringNode(String n) {
        super();
        this.text = n.replace("\"", "").replace("\\", "");
    }

    @Override
    public ExpressionValue evaluate() throws Exception {
        return new StringValue(this.text);
    }
}
