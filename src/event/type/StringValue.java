package event.type;

/**
 * Created by Franklin on 12-13-14.
 */
public class StringValue extends ExpressionValue{
    String text;
    public StringValue(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
