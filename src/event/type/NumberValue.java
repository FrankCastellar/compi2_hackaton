package event.type;

/**
 * Created by Franklin on 12-13-14.
 */
public class NumberValue extends ExpressionValue {
    Double number;
    public NumberValue(Double number){
        this.number = number;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }
}
